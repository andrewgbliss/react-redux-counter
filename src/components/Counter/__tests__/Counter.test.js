import React from 'react';
import Counter from '../Counter';
import renderer from 'react-test-renderer';

it('Counter renders correctly', () => {
  const tree = renderer
    .create(
      <Counter
        key="0"
        value={0}
        onIncrement={() => {}}
        onDecrement={() => {}}
        removeCounter={() => {}}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
