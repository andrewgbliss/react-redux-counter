import React from 'react';
import PropTypes from 'prop-types';

const Counter = ({ value, onIncrement, onDecrement, removeCounter }) => (
  <div>
    <h1>{value}</h1>
    <button onClick={onIncrement}>+</button>
    <button onClick={onDecrement}>-</button>
    <button onClick={removeCounter}>Remove Counter</button>
  </div>
);

Counter.propTypes = {
  value: PropTypes.number.isRequired,
  onIncrement: PropTypes.func.isRequired,
  onDecrement: PropTypes.func.isRequired,
  removeCounter: PropTypes.func.isRequired,
};

export default Counter;
