import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../CountersActions';
import * as types from '../../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Counters Actions', () => {
  it('should create an action to increment a counter', () => {
    const index = 0;
    const expectedActions = [
      {
        type: types.INCREMENT,
        index,
      },
    ];
    const store = mockStore([0]);
    store.dispatch(actions.Increment(index));
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('should create an action to decrement a counter', () => {
    const index = 0;
    const expectedActions = [
      {
        type: types.DECREMENT,
        index,
      },
    ];
    const store = mockStore([0]);
    store.dispatch(actions.Decrement(index));
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('should create an action to add a counter', () => {
    const expectedActions = [
      {
        type: types.ADD_COUNTER,
      },
    ];
    const store = mockStore([0]);
    store.dispatch(actions.AddCounter());
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('should create an action to remove a counter', () => {
    const index = 0;
    const expectedActions = [
      {
        type: types.REMOVE_COUNTER,
        index,
      },
    ];
    const store = mockStore([0]);
    store.dispatch(actions.RemoveCounter(index));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
