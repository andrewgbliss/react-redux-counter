import { combineReducers } from 'redux';
import CountersReducer from './CountersReducer';

export default combineReducers({
  CountersReducer,
});
