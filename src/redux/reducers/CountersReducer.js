import * as types from '../constants/ActionTypes';

const CountersReducer = (state = [0], action) => {
  switch (action.type) {
    case types.INCREMENT:
      return [
        ...state.slice(0, action.index),
        state[action.index] + 1,
        ...state.slice(action.index + 1),
      ];
    case types.DECREMENT:
      return [
        ...state.slice(0, action.index),
        state[action.index] - 1,
        ...state.slice(action.index + 1),
      ];
    case types.ADD_COUNTER:
      return [...state, 0];
    case types.REMOVE_COUNTER:
      return [
        ...state.slice(0, action.index),
        ...state.slice(action.index + 1),
      ];
    default:
      return state;
  }
};

export default CountersReducer;
